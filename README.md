# Welcome to _pluskwa_
_pluskwa_ is an node.js app which serves the thermometer view at [123k.de](http://123k.de). It consumes MQTT messages from an event source you can find at Bitbucket - [https://bitbucket.org/efwe/cimon](https://bitbucket.org/efwe/cimon).

The main aspect of this POC was to change from a poll to a push semantic as early as possible. So here we have a poll on the USB device on the raspberry in my office. This reads the current temperature and stores it. Then there are the node.js timerjobs which are pushing this temperature to the frontend via MQTT (all that happens at _cimon_). The frontend routes these values to any connected websocket. With the message semantics in the middle we're actually more than save that we are developing a event-based application and not a boring request/response cycle again.

Please do not try to think longer than 5 secons about how useful it is to send the office-temperature around the world. But a big shout out goes to the great guys from [obelisk.js](https://github.com/nosir/obelisk.js/) - this is the reason why this POC is super-fancy!

Have any suggestions? 
Tell me about it on Twitter - [@\_efwe\_](https://twitter.com/_efwe_). And always remember _Don't forget to have fun!_ 

Note: I'm also an alpha-tester for the cool guys at [resin.io](http://resin.io) - check them out.