Db = require('mongodb').Db
MongoClient = require('mongodb').MongoClient
Server = require('mongodb').Server
ObjectID = require('mongodb').ObjectID
moment = require('moment')


map = ->
  key = {
    d: new Date(
      this.time.getFullYear(),
      this.time.getMonth(),
      this.time.getDate(),
      this.time.getHours(),
      0, 0, 0),
    l: this.location
    }
  value = {
    total: this.temp,
    count: 1,
    max: null,
    min: null,
    mean: 0,
    temp: this.temp
    ts: new Date()
  }
  emit(key,value)


reduce = (key, values)  ->
  r = {
    total: 0,
    count: 0,
    max: 0,
    min: 50,
    mean: 0,
    ts: null
  }
  for value in values
    r.total += value.total
    r.count += value.count
    if value.temp < r.min
      r.min = value.temp
    if value.temp > r.max
      r.max = value.temp
  return r

finalize = (key, value) ->
  if value.count >0
    value.mean = value.total / value.count
  value.ts = new Date()
  return value


db = new Db('pluskwa', new Server('localhost', 27017),{native_parser: true, w: 0})
db.open (err, db) =>
  if err
    console.log "error: #{err} - we do nothing"
    return


  cutoff = moment().subtract('hours,',1)
  start = new Date('2014-01-01')
  options = {
    out: {  merge: 'temperature.hourly'},
    query: { 'time': { '$gt': start } },
    #query: {},
    finalize: finalize
  }

  collection = db.collection("temperature")
  try
    collection.mapReduce map, reduce, options, (err, collection) ->
      if err
        console.log "problems while mapreducing #{err}"
      else
        console.log "aggregated everything"
  last_run = cutoff

# {"location":"office","time":"2014-06-25T10:17:33.945Z","temp":24.25}
