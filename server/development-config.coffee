config = {
  mongo:{
    host: 'localhost',
    port: 27017
  },
  winston:{
    level: 'debug'
  },
  express:{
    port:5000
  },
  mqtt:{
      host: 'localhost',
      port: 1883,
      settings:{
        keepalive: 1000,
        protocolId: 'MQIsdp',
        protocolVersion: 3,
        clientId: 'pluskwa'
      }
    }
  }

module.exports=config
