express = require('express')
winston = require('winston')

http = require('http')
path = require('path')
io = require('socket.io')
mqtt = require('mqtt')
TemperaturePushHandler = require( __dirname + '/handler/TemperaturePushHandler')
TemperatureStoreHandler = require( __dirname + '/handler/TemperatureStoreHandler')

class Pluskwa
  env = process.env.NODE_ENV || 'development'
  config = require(__dirname + '/'+env+'-config')
  logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({ level: config.winston.level, colorize: true, timestamp: true })
    ]
  })

  constructor: () ->
    mqtt = mqtt.createClient(config.mqtt.port, config.mqtt.host, config.mqtt.settings);
    mqtt.subscribe('temp-stream')
    @temperaturePushHandler = new TemperaturePushHandler(mqtt)
    @temperatureStoreHandler = new TemperatureStoreHandler(mqtt)

  boot: () ->
    app = express()
    app.set('port', config.express.port)
    app.set('views', __dirname + '/views')
    app.set('view engine', 'jade')
    app.use(express.static(path.join(__dirname, 'public')))

    # deliver the index.html
    app.get '/', (req, res) ->
      res.render 'index'
    # register the history-request
    app.get '/history', TemperatureStoreHandler.readHistory

    server = http.createServer(app).listen app.get('port'), () ->
      logger.info "pluskwa booted (#{env}) - listening on port #{app.get('port')}"

    io = io.listen(server)
    io.sockets.on 'connection', (socket) =>
      @temperaturePushHandler.pushTemperature(socket)

module.exports=Pluskwa
