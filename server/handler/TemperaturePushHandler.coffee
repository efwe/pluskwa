winston = require('winston')
mqtt = require('mqtt')

class TemperaturePushHandler
  env = process.env.NODE_ENV || 'development'
  config = require(__dirname + '/../'+env+'-config')
  logger = new (winston.Logger)({
      transports: [
        new (winston.transports.Console)({ level: config.winston.level, colorize: true, timestamp: true })
      ]
    })

  constructor: (@mqtt) ->
    # register one handler which remembers the last message here
    logger.debug "subscribed to 'temp-stream'"
    @mqtt.on 'message', (topic, message) =>
      try
        if topic is 'temp-stream'
          #console.log 'try to read message: '+message
          @lastTemperature = JSON.parse message
          logger.debug "got MQTT message: #{message}"
      catch error
        logger.error "error parsing MQTT message: #{error}"

  pushTemperature: (socket) ->
    # first push the last temp-message we've seen
    if @lastTemperature?
      socket.emit 'temperature', @lastTemperature
      logger.info "pushed last temp: #{@lastTemperature.temp} to new client"
    else
      logger.warn 'no last temperature available - event-source may be down!'
    # each additional temp-message get routed
    @mqtt.on 'message', (topic, message) =>
      try
        if topic is 'temp-stream'
          payload = JSON.parse message
          socket.emit 'temperature', payload
          logger.debug "routed MQTT message to client: #{message}"
      catch error
        logger.error "error routing mqtt message: #{error}"

module.exports=TemperaturePushHandler
