MongoClient = require('mongodb').MongoClient
ObjectID = require('mongodb').ObjectID
winston = require('winston')
mqtt = require('mqtt')
moment = require('moment')


class TemperatureStoreHandler
  MINUTE = 60 * 1000
  HOUR =  60 * MINUTE
  env = process.env.NODE_ENV || 'development'
  config = require(__dirname + '/../'+env+'-config')
  logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({ level: config.winston.level, colorize: true, timestamp: true })
    ]
  })

  db = null
  lastRun = null

  map = ->
    key = {
      d: new Date(
        this.time.getFullYear(),
        this.time.getMonth(),
        this.time.getDate(),
        this.time.getHours(),
        0, 0, 0),
      l: this.location
      }
    value = {
      total: this.temp,
      count: 1,
      max: null,
      min: null,
      mean: 0,
      temp: this.temp
      ts: new Date()
    }
    emit(key,value)


  reduce = (key, values)  ->
    r = {
      total: 0,
      count: 0,
      max: 0,
      min: 50,
      mean: 0,
      ts: null
    }
    for value in values
      r.total += value.total
      r.count += value.count
      if value.temp < r.min
        r.min = value.temp
      if value.temp > r.max
        r.max = value.temp
    return r

  finalize = (key, value) ->
    if value.count >0
      value.mean = value.total / value.count
    value.ts = new Date()
    return value

  aggregateHourly = (db) =>
    if lastRun?
      start = lastRun
      logger.debug "last run saved - we start @ #{start.format()}"
    else
      start = moment("2014-01-01")
      logger.debug "no last run - we start @ #{start.format()}"

    cutoff = moment().subtract('minutes,',5)
    options = {
      out: {  merge: 'temperature.hourly'},
      query: { time: { '$gt': start._d, '$lt': cutoff._d } },
      finalize: finalize
    }

    collection = db.collection("temperature")
    collection.mapReduce map, reduce, options, (err, collection) ->
      if err
        logger.error "problems while mapreducing #{err}"
      else
        logger.info "aggregated everything"
        lastRun = cutoff

  @readHistory = (req, res) ->
    #db.temperature.hourly.find({'_id.l':'office'}).sort({'_id.d':-1}).limit(3)
    collection = db.collection("temperature.hourly")
    collection.find({'_id.l':'office'}, {sort:{'_id.d':-1}, limit:3}, fields: {'value.mean':1, '_id':0}).toArray (err, docs)->
        if err
          logger.error "unable to query temperatures #{err}"
        else
          # right now we really only need the temperature at the client => transform once more
          result = []
          for doc in docs
            result.push doc.value.mean
          res.send result

  constructor: (mqtt) ->
    # create a db connection for this store-instance
    MongoClient.connect "mongodb://#{config.mongo.host}:#{config.mongo.port}/pluskwa", (err, database) ->
        if err
          throw err
        db = database

        # register a handler which stores everything in mongo
        mqtt.on 'message', (topic, message) ->
          try
            if topic is 'temp-stream'
              #logger.debug "try to read message #{message}"
              collection = database.collection("temperature")
              document = JSON.parse(message)
              document.time = new Date(document.time)
              document._id = new ObjectID()
              collection.insert document, {w: 1}, (err, records) ->
                if err
                  logger.error "unable to store temperature #{err}"
                else
                  logger.debug 'stored temperature'
          catch error
            logger.error "unable to store mqtt message #{error}"

        # schedule the aggregation timer
        setInterval aggregateHourly, HOUR, database

module.exports=TemperatureStoreHandler
