Db = require('mongodb').Db
MongoClient = require('mongodb').MongoClient
Server = require('mongodb').Server
ObjectID = require('mongodb').ObjectID
moment = require('moment')

db = new Db('pluskwa', new Server('localhost', 27017),{native_parser: true, w: 0})
db.open (err, db) =>
  if err
    console.log "error: #{err} - we do nothing"
    return

  message = {
      "location":"office",
      "time":"2014-06-25T10:17:33.945Z",
      "temp":24.25
    }

  collection = db.collection("temperature")
  baseDate = moment()
  tempOffset=0
  for i in [0..800] by 1
    baseDate.add('seconds',15)
    message._id = new ObjectID()
    message.time = new Date(baseDate.format())
    message.temp = 19+(tempOffset%4)+(tempOffset%8/8)
    tempOffset++
    collection.insert message, {w: 1}, (err, records) ->
        if err
          console.log "error #{err}"
