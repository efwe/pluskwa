class Pluskwa

  constructor: () ->
    @obelometer = new Obelometer()
    @socket = io()
    @socket.on 'temperature', (data) =>
        console.log 'temperature', data
        @obelometer.showTemperature(data)
